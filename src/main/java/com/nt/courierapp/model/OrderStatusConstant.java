package com.nt.courierapp.model;

public enum OrderStatusConstant {
    PENDING, COMPLETED, ONGOING, REJECTED
}
