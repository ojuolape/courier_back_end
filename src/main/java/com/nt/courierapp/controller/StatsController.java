package com.nt.courierapp.controller;

import com.nt.courierapp.message.response.order.OrderStatsDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("stats")
public class StatsController {

    @GetMapping("orders")
    public OrderStatsDto fetchOrderStats(Principal principal){
        return new OrderStatsDto(10, 21000.0, 1);
    }
}
