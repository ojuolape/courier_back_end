package com.nt.courierapp.controller;

import com.nt.courierapp.message.response.QueryResult;
import com.nt.courierapp.message.response.order.OrderDto;
import com.nt.courierapp.model.OrderStatusConstant;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/orders")
public class OrderController {

    @GetMapping("")
    public QueryResult fetchOrders() {
        List<OrderDto> orderDtos = new ArrayList<>();
        OrderDto orderDto = new OrderDto();
        orderDto.setAddress("No 12 YPO Shodeinde Street");
        orderDto.setCustomerName("Animashaun Badmus");
        orderDto.setOrderId("ABSDAS");
        orderDto.setStatus(OrderStatusConstant.PENDING);
        orderDtos.add(orderDto);
        orderDto = new OrderDto();
        orderDto.setAddress("No 12 YPO Shodeinde Street");
        orderDto.setCustomerName("Animashaun Badmus");
        orderDto.setOrderId("ABSDAS");
        orderDto.setStatus(OrderStatusConstant.PENDING);
        orderDtos.add(orderDto);
        orderDto = new OrderDto();
        orderDto.setAddress("No 12 YPO Shodeinde Street");
        orderDto.setCustomerName("Animashaun Badmus");
        orderDto.setOrderId("ABSDAS");
        orderDto.setStatus(OrderStatusConstant.PENDING);
        orderDtos.add(orderDto);
        orderDto = new OrderDto();
        orderDto.setAddress("No 12 YPO Shodeinde Street");
        orderDto.setCustomerName("Animashaun Badmus");
        orderDto.setOrderId("ABSDAS");
        orderDto.setStatus(OrderStatusConstant.PENDING);
        orderDtos.add(orderDto);
        orderDto = new OrderDto();
        orderDto.setAddress("No 12 YPO Shodeinde Street");
        orderDto.setCustomerName("Animashaun Badmus");
        orderDto.setOrderId("ABSDAS");
        orderDto.setStatus(OrderStatusConstant.PENDING);
        orderDtos.add(orderDto);
        orderDto = new OrderDto();
        orderDto.setAddress("No 12 YPO Shodeinde Street");
        orderDto.setCustomerName("Animashaun Badmus");
        orderDto.setOrderId("ABSDAS");
        orderDto.setStatus(OrderStatusConstant.PENDING);
        orderDtos.add(orderDto);
        orderDto = new OrderDto();
        orderDto.setAddress("No 12 YPO Shodeinde Street");
        orderDto.setCustomerName("Animashaun Badmus");
        orderDto.setOrderId("ABSDAS");
        orderDto.setStatus(OrderStatusConstant.PENDING);
        orderDtos.add(orderDto);
        orderDto = new OrderDto();
        orderDto.setAddress("No 12 YPO Shodeinde Street");
        orderDto.setCustomerName("Animashaun Badmus");
        orderDto.setOrderId("ABSDAS");
        orderDto.setStatus(OrderStatusConstant.PENDING);
        orderDtos.add(orderDto);
        return new QueryResult(10, 0, orderDtos);
    }

}
