package com.nt.courierapp.message.response.order;

import com.nt.courierapp.model.OrderStatusConstant;

public class OrderDto {
    private String orderId;
    private String address;
    private String customerName;
    private OrderStatusConstant status;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public OrderStatusConstant getStatus() {
        return status;
    }

    public void setStatus(OrderStatusConstant status) {
        this.status = status;
    }
}
