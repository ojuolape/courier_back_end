package com.nt.courierapp.message.response;

import java.util.List;

public class QueryResult<E> {
    private int limit;
    private int offset;
    private List<E> result;

    public QueryResult(int limit, int offset, List<E> result) {
        this.limit = limit;
        this.offset = offset;
        this.result = result;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public List<E> getResult() {
        return result;
    }

    public void setResult(List<E> result) {
        this.result = result;
    }
}
